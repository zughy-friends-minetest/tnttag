minetest.register_entity("tnttag:tnt_if_tagged", {
	initial_properties = {
		static_save = false,
		collide_with_objects = false,
		pointable = false,
		collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
        selectionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
		visual = "cube",
		visual_size = {x = .5, y = .5, z = .5},
		textures = {"tnttag_tnt_top_burning.png", "tnttag_tnt_bottom.png", "tnttag_tnt_side.png", "tnttag_tnt_side.png", "tnttag_tnt_side.png", "tnttag_tnt_side.png"},
	},
	_timer = 0,
	on_activate = function(self)
		self.object:set_armor_groups({immortal=1})
	end,
	on_step = function(self, dtime, moveresult)
		if self._timer > .3 and self.object:get_attach() == nil then
			self.object:remove()
		else
			self._timer = self._timer + dtime
		end
	end,
})
