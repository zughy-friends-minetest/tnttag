local S = minetest.get_translator("tnttag")

-- How many seconds need to be left on the TNT clock
-- to play a countdown beep each second for the tagger(s)
local COUNTDOWN_SECONDS = 10

-- Sound gain for TNT countdown sound (tagger)
local GAIN_TIMER = 0.7
-- Sound gain for TNT countdown sound (non-tagger)
local GAIN_TIMER_NONTAGGER = 0.4

local has_audio_lib = minetest.get_modpath("audio_lib") ~= nil
if has_audio_lib then
	-- TNT alert sound when time is running out (for tagger)
	audio_lib.register_sound("sfx", "tnttag_timer", S("Alert"), {gain=GAIN_TIMER})
	-- Same, but for non-taggers
	audio_lib.register_sound("sfx", "tnttag_timer_nontagger", S("Countdown"), {gain=GAIN_TIMER_NONTAGGER})
end

arena_lib.on_enable("tnttag", function(arena, p_name)
	-- Check for invalid arena properties
	local error_msg
	if arena.wavetime < 1 then
		error_msg = S("Arena property \"wavetime\" cannot be smaller than 1!")
	elseif arena.pause_length < 0 then
		error_msg = S("Arena property \"pause_length\" cannot be negative!")
	end
	if error_msg then
		minetest.chat_send_player(p_name, error_msg)
		return false
	end
	return true
end)

arena_lib.on_load("tnttag", function(arena)
	arena.original_player_amount = arena.players_amount
	arena.waves = arena.players_amount - 1
	for p_name, stats in pairs(arena.players) do
		tnttag.generate_HUD(arena, p_name)
	end
end)

arena_lib.on_start("tnttag", function(arena)
	arena.current_time = arena.wavetime*arena.waves
	tnttag.newwave(arena)
end)

arena_lib.on_join("tnttag", function(p_name, arena, as_spectator)
	if as_spectator then
		tnttag.generate_HUD(arena, p_name)
		tnttag.update_pause_hud(arena)
		tnttag.update_wave_timer_hud(arena)
	end
end)

arena_lib.on_quit("tnttag", function(arena, p_name, is_spectator, reason)
	if not is_spectator then
		tnttag.remove_tnthead(p_name)
		tnttag.update_player_count_hud(arena)
	end
	tnttag.remove_HUD(arena, p_name)
end)

arena_lib.on_eliminate("tnttag", function(arena, p_name)
	tnttag.remove_tnthead(p_name)
	tnttag.update_player_count_hud(arena)
end)

arena_lib.on_celebration("tnttag", function(arena, winner)
	arena.current_time = 0
	for p_name in pairs(arena.players) do
		tnttag.remove_tnthead(p_name)
	end
	-- Celebration counts as "pause"
	arena.pause = true
	tnttag.update_pause_hud(arena)
	tnttag.update_wave_timer_hud(arena)
end)

arena_lib.on_end("tnttag", function(arena, winners, is_forced)
	for psp_name in pairs(arena.players_and_spectators) do
		tnttag.remove_HUD(arena, psp_name)
	end
end)

arena_lib.on_time_tick("tnttag", function(arena)
	local newwave = false
	arena.wave_just_started = false
	if arena.pause then
		if arena.current_time%arena.wavetime == 0 then
			tnttag.newwave(arena)
			newwave = true
		end
	else
		local taggersnum = 0
		for p_name,stats in pairs(arena.players) do
			if tnttag.gettagstatus(p_name, arena) then taggersnum = taggersnum + 1 end
		end
		if taggersnum == 0 then
			arena.current_time=(arena.current_time-arena.current_time%arena.wavetime)-1
			tnttag.newwave(arena)
			newwave = true
		end
		local wavetimeleft = arena.current_time%arena.wavetime
		if wavetimeleft == 0 then
			for p_name in pairs(arena.players) do
				if arena.players[p_name].tagged then
					tnttag.explode_player(p_name, arena)
				end
			end
			arena.current_time = arena.current_time + arena.pause_length
			if arena.pause_length == 0 then
				tnttag.newwave(arena)
				newwave = true
			else
				arena.pause = true
				tnttag.update_pause_hud(arena)
			end
		-- Play warning sound each second to players when time is running out
		elseif wavetimeleft <= COUNTDOWN_SECONDS then
			local play_alert_to, play_countdown_to = {}, {}
			for p_name in pairs(arena.players) do
				-- Loud "alert" sound for tagger
				if arena.players[p_name].tagged then
					play_alert_to[p_name] = true
				-- Short subtle bleep sound for everyone else
				else
					play_countdown_to[p_name] = true
				end
			end
			-- Also play sound to spectators
			for p_name in pairs(arena.spectators) do
				local target = arena_lib.get_spectated_target(p_name)
				if target and target.type == "player" then
					if arena.players[target.name].tagged then
						play_alert_to[p_name] = true
					else
						play_countdown_to[p_name] = true
					end
				end
			end
			if has_audio_lib then
				audio_lib.play_sound("tnttag_timer", {to_players=play_alert_to})
				audio_lib.play_sound("tnttag_timer_nontagger", {to_players=play_countdown_to})
			else
				for p_name, _ in pairs(play_alert_to) do
					minetest.sound_play({name="tnttag_timer", gain=GAIN_TIMER}, {to_player=p_name}, true)
				end
				for p_name, _ in pairs(play_countdown_to) do
					minetest.sound_play({name="tnttag_timer", gain=GAIN_TIMER}, {to_player=p_name}, true)
				end
			end
		end
	end
	if not newwave then
		tnttag.update_wave_timer_hud(arena)
	end
	tnttag.update_player_count_hud(arena)
end)

arena_lib.on_timeout("tnttag", function(arena)
	local winners = {}
	for p_name in pairs(arena.players) do
		if arena.players[p_name].tagged then
			tnttag.explode_player(p_name, arena)
			tnttag.update_player_count_hud(arena)
		else
			table.insert(winners, p_name)
		end
	end
	if (#winners > 1) then
		arena_lib.load_celebration("tnttag", arena, table.concat(winners,", "))
	end
	tnttag.update_wave_timer_hud(arena)
end)
