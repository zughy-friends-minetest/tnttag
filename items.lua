local S = minetest.get_translator("tnttag")

local has_audio_lib = minetest.get_modpath("audio_lib") ~= nil
if has_audio_lib then
	-- When you got tagged with TNT
	audio_lib.register_sound("sfx", "tnttag_tag_gotten", S("Got tagged"), {gain = 1.0})
	-- When you tag other player with TNT
	audio_lib.register_sound("sfx", "tnttag_tag_given", S("Tagged player"), {gain=1.0})
end

-- The item used to tag other players with TNT.
-- Note: This MUST be registered as a node, the particles depend on it.
minetest.register_node("tnttag:tnt", {
	description = S("TNTTagger - Tag other players"),
	tiles = {"tnttag_tnt_top_burning.png", "tnttag_tnt_bottom.png", "tnttag_tnt_side.png"},
	-- Allow it to be dug
	groups = { choppy = 1, not_in_creative_inventory = 1 },
	node_placement_prediction = "",
	on_place = function(itemstack, placer, pointed_thing)
		local placer_name = placer:get_player_name()
		if arena_lib.is_player_in_arena(placer_name, "tnttag") then
			return nil
		else
			return minetest.item_place_node(itemstack, placer, pointed_thing)
		end
	end,
	on_drop = function(itemstack, dropper, pos)
		local dropper_name = dropper:get_player_name()
		if arena_lib.is_player_in_arena(dropper_name, "tnttag") then
			return nil
		else
			return minetest.item_drop(itemstack, dropper, pos)
		end
	end,
	on_use = function(itemstack, user, pointed_thing)
		local user_name = user:get_player_name()
		if arena_lib.is_player_in_arena(user_name, "tnttag") then
			if pointed_thing.type == "object" then
				if not pointed_thing.ref:get_luaentity() then
					local player = pointed_thing.ref
					local p_name = player:get_player_name()
					if arena_lib.is_player_in_arena(p_name, "tnttag") then
						local arena = arena_lib.get_arena_by_player(user_name)
						if not arena.players[p_name].tagged then
							-- Chat messages
							for pl_name, _ in pairs(arena.players_and_spectators) do
								minetest.chat_send_player(pl_name, S("@1 tagged @2", user_name, p_name))
							end
							-- Title messages with sound
							local sound_gotten, sound_given
							if has_audio_lib then
								sound_gotten = "tnttag_tag_gotten"
								sound_given = "tnttag_tag_given"
							else
								sound_gotten = {name="tnttag_tag_gotten", gain=1}
								sound_given = {name="tnttag_tag_given", gain=1}
							end
							-- Title message to tagger
							arena_lib.HUD_send_msg("title", p_name, S("You have been tagged by @1!", user_name),1, sound_gotten, 0xFF3300)
							-- Titte message to tagged player
							arena_lib.HUD_send_msg("title", user_name, S("You tagged @1!", p_name),1, sound_given, 0x33FF33)

							-- Show similar title messages to spectators of tagger and tagged player
							local tagger_spectators = arena_lib.get_player_spectators(p_name)
							for sp_name, _ in pairs(tagger_spectators) do
								arena_lib.HUD_send_msg("title", sp_name, S("@1 got tagged by @2", p_name, user_name), 1, sound_gotten, 0xFF3300)
							end
							local tagged_spectators = arena_lib.get_player_spectators(user_name)
							for sp_name, _ in pairs(tagged_spectators) do
								arena_lib.HUD_send_msg("title", sp_name, S("@1 tagged @2", user_name, p_name), 1, sound_given, 0x33FF33)
							end

							-- Update tag status
							tnttag.tagplayer(p_name, arena)
							tnttag.untagplayer(user_name, arena)
						end
					end
				end
			end
		end
	end,
})

tnttag.tagitem = "tnttag:tnt"
