local S = minetest.get_translator("tnttag")
local NS = function(s) return s end

-- HACK: Repeat mod.conf description here so that string collecting utils can find it
NS("A player is holding TNT which is about to explode and must tag other players to get rid of it")

tnttag = {}
tnttag.saved_huds = {}

tnttag.player_speed = minetest.settings:get("tnttag.player_speed") or 3
tnttag.player_jump = minetest.settings:get("tnttag.player_jump") or 1.2
tnttag.player_speed_tagged = minetest.settings:get("tnttag.player_speed_tagged") or 3
tnttag.player_jump_tagged = minetest.settings:get("tnttag.player_jump_tagged") or 1.2

arena_lib.register_minigame("tnttag", {
	name = S("TNTTag"),
	prefix = "["..S("TNTTag").."] ",
	min_players = 2,
    icon = "tnttag.png",
	properties = {
		wavetime = 30, -- time in seconds per wave
		pause_length = 5, -- time in seconds for a pause, set to 0 for none
	},
	temp_properties = {
		waves = 1,
		current_wave = 0,
		wave_just_started = false,
		original_player_amount = 0,
		pause = true,
	},
	player_properties = {
		tagged = false,
		tnt_if_tagged = nil,
	},
	in_game_physics = {
		speed = tnttag.player_speed,
		jump = tnttag.player_jump,
		sneak_glitch = true,
		new_move = false,
	},
	hotbar = {
      slots = 1,
      background_image = "tnttag_gui_hotbar.png"
    },
	-- Players mustn't die the "traditional" way in this minigame,
	-- only by TNT explosions. This implies maps cannot have death traps.
	disabled_damage_types = {"fall", "punch", "drown", "node_damage", "set_hp"},
	hud_flags = { minimap = false, healthbar = false, breathbar = false },
	disable_inventory = true,
	celebration_time = 10,
	time_mode = "decremental",
	sounds = {
		eliminate = false,
	},
})

-- Disable /kill for players in TNTTag because we disable
-- the set_hp damage type.
minetest.register_on_chatcommand(function(name, command, params)
	if command ~= "kill" then
		return
	end
	local killer = name
	local victim
	if params == "" then
		victim = name
	else
		victim = params
	end
	if arena_lib.is_player_playing(victim, "tnttag") then
		minetest.chat_send_player(killer, S("The `kill` command is not permitted in TNTTag."))
		return true
	end
end)

dofile(minetest.get_modpath("tnttag") .. "/api.lua")
dofile(minetest.get_modpath("tnttag") .. "/items.lua")
dofile(minetest.get_modpath("tnttag") .. "/entities.lua")



-- Thankyou to chmodsayshello ↑

dofile(minetest.get_modpath("tnttag") .. "/hud.lua")
dofile(minetest.get_modpath("tnttag") .. "/auto.lua")

minetest.register_privilege("tnttag_admin", S("Needed for TNTTag"))
