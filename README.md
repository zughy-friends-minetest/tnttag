# TNTTag

## Gameplay

At the start of the game, one or more players are chosen
at random. These players are forced to carry TNT which
will explode soon! Players carrying TNT are called "taggers".
As a tagger, you have to chase any other player and
punch them so they have to carry the TNT instead.
When the TNT timer reaches 0, it will explode together with
the tagger holding. The tagger is eliminated.

Once the TNT has gone boom, there's a short pause, then
the next "wave" begins with new player(s) being chosen
at random to become taggers.

Players will explode that way, one after another, until only
one is left. The last player standing wins the game.

## HUD indicators

On the top of the screen, there are several indicators in the
Heads-Up Display (HUD) showing info about the game.

From left to right:

1. Status: Shows the current game status:
    * Play icon: Active TNT in the game, it will blow up soon!
    * Pause icon: A short break before the next wave begins
    * Trophy icon: We have a winner!
2. Player count: Shows the number of remaining and total players
3. TNT timer: How many seconds remain on the TNT timer or of the current pause
4. Wave: Shows the current and final wave (i.e. round)

## Setting up

### Create an own arena

Use this chat command:

`/arenas create tnttag <arena name> <min. players> <max. players>`

Then edit the arena using:

`/arenas edit tnttag <arena name>`

### Arena properties

The following arena properties exist:

* `wavetimer`: Time in seconds for a single wave (until the TNT explodes) (default: 30)
* `pause_length`: Length of a pause between waves, in seconds. Set to 0 to disable pauses (default: 5)

### Schematics for your arenas

Find nice arenas here: <https://codeberg.org/debiankaios/tnttag_arenas>

### Edit your own arena

#### Warning about timers

Don't set the arena timer. It won't change anything. Set the `wavetime` in
the arena settings instead for the time of each wave.

## Licensing / Credits

Main license: See `LICENSE.txt`.
Media license: See `LICENSE_MEDIA.txt` and `sounds/LICENSE.txt`.
