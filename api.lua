local S = minetest.get_translator("tnttag")

local has_audio_lib = minetest.get_modpath("audio_lib") ~= nil
if has_audio_lib then
	audio_lib.register_sound("sfx", "tnttag_tnt_explode", S("Explosion"), {gain = 1.0, max_hear_distance = 128})
	audio_lib.register_sound("sfx", "tnttag_new_round", S("New round"), {gain=1.0})
end

-- TNT break particles (for TNT above player head)
local function tnt_particles(pos)
	minetest.add_particlespawner({
		amount = 16,
		time = 0.01,
		pos = {
			min = vector.add(pos, vector.new(-0.2, -0.2, -0.2)),
			max = vector.add(pos, vector.new(0.2, 0.2, 0.2)),
		},
		vel = {
			min = vector.new(-1, -0.2, -1),
			max = vector.new(1, 0.2, 1),
		},
		acc = vector.new(0, -1, 0),
		exptime = { min = 0.2, max = 0.7 },
		size = { min = 0.25, max = 0.5 },
		collisiondetection = false,
		vertical = false,
		node = {name="tnttag:tnt"},
	})
end

-- Spawns explosion particles at pos
local function explosion_particles(pos)
	local radius = 3
	minetest.add_particlespawner({
		amount = 128,
		time = 0.6,
		pos = {
			min = vector.subtract(pos, radius / 2),
			max = vector.add(pos, radius / 2),
		},
		vel = {
			min = vector.new(-20, -20, -20),
			max = vector.new(20, 20, 20),
		},
		acc = vector.zero(),
		exptime = { min = 0.2, max = 1.0 },
		size = { min = 16, max = 24 },
		drag = vector.new(1,1,1),
		texture = {
			name = "tnttag_smoke_anim_1.png", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
			name = "tnttag_smoke_anim_2.png", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
			name = "tnttag_smoke_anim_1.png^[transformFX", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
			name = "tnttag_smoke_anim_2.png^[transformFX", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
		},
	})
	minetest.add_particlespawner({
		amount = 1,
		time = 0.01,
		pos = pos,
		vel = vector.zero(),
		acc = vector.zero(),
		exptime = 1,
		size = radius*10,
		texture = "tnttag_smoke_ball_big.png",
		animation = { type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = -1, },
	})
	minetest.add_particlespawner({
		amount = 4,
		time = 0.25,
		pos = pos,
		vel = vector.zero(),
		acc = vector.zero(),
		exptime = { min = 0.6, max = 0.9 },
		size = { min = 8, max = 12 },
		radius = { min = 0.5, max = math.max(0.6, radius*0.75) },
		texture = "tnttag_smoke_ball_medium.png",
		animation = { type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = -1, },
	})
	minetest.add_particlespawner({
		amount = 28,
		time = 0.5,
		pos = pos,
		vel = vector.zero(),
		acc = vector.zero(),
		exptime = { min = 0.5, max = 0.8 },
		size = { min = 6, max = 8 },
		radius = { min = 1, max = math.max(1.1, radius+1) },
		texture = "tnttag_smoke_ball_small.png",
		animation = { type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = -1, },
	})
end

-- Spawn black particles to make it look like as if
-- a player was scorched.
-- pos is the player position.
local function scorched_player_particles(pos)
	minetest.add_particlespawner({
		amount = 100,
		time = 0.1,
		pos = {
			min = vector.subtract(pos, vector.new(0.3, 0, 0.3)),
			max = vector.add(pos, vector.new(0.3, 1.78, 0.3)),
		},
		vel = {
			min = vector.new(-0.2, 0.1, -0.2),
			max = vector.new(0.2, 0.1, 0.2),
		},
		collisiondetection = true,
		acc = vector.new(0, -0.5, 0),
		exptime = { min = 4.5, max = 5.5 },
		size = { min = 0.5, max = 1 },
		drag = vector.new(1,0,1),
		texture = "tnttag_scorch.png",
	})
end

-- gives a player the tagging item
function tnttag.add_tager(player)
	player:get_inventory():set_stack("main", 1, tnttag.tagitem)
end

-- takes the tagging item
function tnttag.remove_tager(player)
	local inv = player:get_inventory()
	local stack = ItemStack(tnttag.tagitem)
	local taken = inv:remove_item("main", stack)
end

-- adds the tnt head entity
function tnttag.add_tnthead(p_name)
	local player = minetest.get_player_by_name(p_name)
	if player then
		local tnthead = minetest.add_entity(minetest.get_player_by_name(p_name):get_pos(), "tnttag:tnt_if_tagged", nil)
		-- Find a position to attach the TNT head entity to
		if minetest.get_modpath("player_api") or minetest.get_modpath("default") then
			-- Relative to Head bone if player_api / default mod found
			tnthead:set_attach(player, "Head", {x=0, y=10, z=0})
		else
			-- If we can't make assumptions about the model bones,
			-- the offset is based on the selection box height
			local props = player:get_properties()
			local selbox = props.selbox or { -0.3, 0, -0.3,	0.3, 1.77, 0.3 }
			local offset = selbox[2] + selbox[5] + 0.8
			tnthead:set_attach(player, "", {x=0, y=offset * 10, z=0})
		end
	end
end
-- removes the tnt entity
function tnttag.remove_tnthead(p_name)
	local player = minetest.get_player_by_name(p_name)
	if player then
		local children = player:get_children()
		for _,child in ipairs(children) do
			if child:get_luaentity() and child:get_luaentity().name == "tnttag:tnt_if_tagged" then
				child:set_detach()
                child:remove()
			end
		end
	end
end

-- called when tagged
function tnttag.tagplayer(p_name, arena)
	local player = minetest.get_player_by_name(p_name)
	arena.players[p_name].tagged = true
	tnttag.add_tager(minetest.get_player_by_name(p_name))
    tnttag.add_tnthead(p_name)
	player:set_physics_override({
        speed = tnttag.player_speed_tagged,
		jump = tnttag.player_jump_tagged,
    })
end

-- called when a player tags another player
function tnttag.untagplayer(p_name, arena)
	local player = minetest.get_player_by_name(p_name)
	arena.players[p_name].tagged = false
	tnttag.remove_tager(minetest.get_player_by_name(p_name))
    tnttag.remove_tnthead(p_name)
	player:set_physics_override({
        speed = tnttag.player_speed,
		jump = tnttag.player_jump,
    })
end

-- get the get tagstatus
function tnttag.gettagstatus(p_name, arena)
	return arena.players[p_name].tagged
end

-- Thankyou to chmodsayshello ↓
function tnttag.get_new_tagger_count(arena, num_tagger)
	local players = {}
	local already_tagged = {}
	local players_in_arena = {}
	local count = 0
    for p_name in pairs(arena.players) do
        count = count + 1
		table.insert(players_in_arena, p_name)
    end
    for i = 1,num_tagger do
		local possible_player_index
		local success = false
        while not success do
            possible_player_index = math.random(1,count)
            success = true
            for used in pairs(already_tagged) do
                if possible_player_index == used then
                    success = false
                end
            end
        end
        table.insert(already_tagged,possible_player_index)
        table.insert(players,players_in_arena[possible_player_index])
    end
    return players
end


function tnttag.get_new_tagger(arena, percentage) --same usage: get_new_tagger(arena, 0.25) for 25%
    local count = 0
    for p_name in pairs(arena.players) do
        count = count + 1
    end
    local num_tagger = math.floor(count*percentage)
	if num_tagger < 1 then
		num_tagger = 1
	end
    return tnttag.get_new_tagger_count(arena, num_tagger)
end


function tnttag.explode_player(p_name, arena)
	local player = minetest.get_player_by_name(p_name)

	-- Boom!
	if has_audio_lib then
		audio_lib.play_sound("tnttag_tnt_explode", {object = player})
	else
		minetest.sound_play("tnttag_tnt_explode", {object = player, gain = 1.0, max_hear_distance = 128}, true)
	end

	-- explosion particles
	local ppos = player:get_pos()
	-- center explosion above player head (roughly where the TNT entity is)
	local epos = vector.add(ppos, {x=0, y=2.2, z=0})
	tnt_particles(epos)
	scorched_player_particles(ppos)
	explosion_particles(epos)

	local spectators = arena_lib.get_player_spectators(p_name)

	-- Elimination messages
	-- Title message
	arena_lib.HUD_send_msg("title", p_name, S("You exploded!"),1, nil--[[sound↑]], 0xFF3300)
	for sp_name, _ in pairs(spectators) do
		arena_lib.HUD_send_msg("title", sp_name, S("@1 exploded!", p_name), 1, nil--[[sound↑]], 0xFF3300)
	end
	-- Chat message
	for pl_name,_ in pairs(arena.players_and_spectators) do
		minetest.chat_send_player(pl_name, S("@1 exploded!", p_name))
	end

	-- Add spectate area at explosion position
	-- and make all players currently spectating that player
	-- spectate the area instead
	local s_name = S("@1 (exploded)", p_name)
	arena_lib.add_spectate_area("tnttag", arena, s_name, ppos)
	for spectator, _ in pairs(spectators) do
		arena_lib.spectate_target("tnttag", arena, spectator, "area", s_name)
	end

	-- Eliminate
	arena_lib.remove_player_from_arena(p_name, 1)

	-- Make the exploding player spectate their position of death
	arena_lib.spectate_target("tnttag", arena, p_name, "area", s_name)
end

function tnttag.newwave(arena)
	local player
	local new_tagger
	local real_new_tagger = {}
	arena.current_wave = arena.current_wave+1
	arena.wave_just_started = true
	tnttag.update_wave_counter_hud(arena)
	local p_count = 0
	for _ in pairs(arena.players) do
		p_count = p_count + 1
	end
	if arena.current_wave == arena.waves then --Last wave
		new_tagger = tnttag.get_new_tagger(arena, 0.5)
	else
		new_tagger = tnttag.get_new_tagger(arena, 0.5)
	end
	local count = 0
	for _, p_name in pairs(new_tagger) do
		if p_count-count > (arena.waves-arena.current_wave)+1 then --für die bessere lessbarkeit/for better readability
			tnttag.tagplayer(p_name, arena)
			table.insert(real_new_tagger, p_name)
			count = count + 1
		end
	end
	local player_names = {}
	for p_name in pairs(arena.players_and_spectators) do
		-- list separator for tagger list

		local taggerlist = table.concat(real_new_tagger, ", ")
		local message_spectator
		local message = S("The new tagger is @1!", taggerlist)
		if #real_new_tagger > 1 then
			message = S("The new taggers are @1!", taggerlist)
		end
		minetest.chat_send_player(p_name, message)

		player_names[p_name] = true
		if not has_audio_lib then
			minetest.sound_play("tnttag_new_round", {to_player = p_name, gain = 1.0}, true)
		end

		if #real_new_tagger == 1 then
			message = S("You're the tagger!")
			message_spectator = S("@1 is the tagger!", p_name)
		else
			message = S("You're a tagger!")
			message_spectator = S("@1 is a tagger!", p_name)
		end
		for i=1, #real_new_tagger do
			-- Title message to new tagger(s)
			arena_lib.HUD_send_msg("title", real_new_tagger[i], message, 1, nil, 0xFF3300)
			-- Title message to spectators of the new tagger(s)
			if message_spectator then
				local spectators = arena_lib.get_player_spectators(real_new_tagger[i])
				for sp_name, _ in pairs(spectators) do
					arena_lib.HUD_send_msg("title", sp_name, message_spectator, 1, nil, 0xFF3300)
				end
			end
		end
	end
	if has_audio_lib then
		audio_lib.play_sound("tnttag_new_round", {to_players = player_names}, true)
	end
	for p_name in pairs(arena.players) do
		player = minetest.get_player_by_name(p_name)
		arena_lib.teleport_onto_spawner(player, arena) --WARNING: Tagged and Untagged player on same spawn-point
	end
	arena.pause = false
	tnttag.update_pause_hud(arena)
	tnttag.update_wave_timer_hud(arena)

	-- Remove all spectatable areas to force spectators to
	-- spectate a remaining player again
	local s_areas = arena_lib.get_spectatable_areas("tnttag", arena.name)
	for area_name, _ in pairs(s_areas) do
		arena_lib.remove_spectate_area("tnttag", arena, area_name)
	end
end
